# Visualização de dados
## Integrantes
* Luciano Kelvin da Silva ([kelvin@ime.usp.br](mailto:kelvin@ime.usp.br))
* Gabriely Rangel Pereira ([gabriely.pereira@usp.br](mailto:gabriely.pereira@usp.br))
* Bruna Regina Coelho ([bruna.coelho@usp.br](mailto:bruna.coelho@usp.br))
* Alexandre Locci Martins ([alexandre.locci@gmail.com](mailto:alexandre.locci@gmail.com))
* Ricardo Akira Tanaka ([raktanaka@gmail.com](mailto:raktanaka@gmail.com))

## Descrição
O objetivo do projeto é criar um _dashboard_ com análises para sistemas de código aberto, como o kernel do linux. Existe um interesse em obter análises como: tempo médio de duração das conversas por e-mail ou IRC, quantidade de usuários participantes do canal ao longo do tempo, região de origem dos contribuintes, entre outros. Esse grupo é responsável pela visualização dos dados, portanto iremos criar uma API que provê vários tipos de gráficos (pizza, barras, linha, entre outros) a partir dos dados obtidos pelos grupos de mineração de dados. Será oferecida uma lista de gráficos disponíveis (ainda a ser definida), em que o grupo de _front-end_ e integração deverá chamar cada gráfico de acordo com a preferência do usuário. Em geral, haverá opções de customização dos gráficos, como tamanho, cor e legenda.

## APIs exigidas
Será necessário acessar as APIs informadas pelos demais grupos e fazer a conexão com as bases de dados para gerar as análises.

## APIs oferecidas
Com base nos dados obtidos pelos outros grupos, vamos disponibilizar análises para personalizar o _dashboard_. A análise será feita em R, usando a gema [RinRuby](https://github.com/clbustos/rinruby) e os gráficos serão construídos em Javascript e devolvidos em HTML. Desta forma, será oferecida uma API que gera gráficos com a métrica e customização desejada em uma página web.

## Principais funcionalidades (requisitos)
* Oferecer de forma simples uma API que desenhe os gráficos em uma página web, sem a necessidade de muitas configurações, facilitando assim a integração com o sistema.
* Fazer gráficos para diferentes tipos de dados do sistema, por exemplo:
	* Usuários com mais commits
	* Evolução temporal do números de commits
	* Quantidade de issues
	* Mapa de palavras mais relevantes em cada projeto
	* Número de commits aprovados ou rejeitados
	* Instituições mais participativas nos projetos
* Permitir a personalização dos gráficos oferecidos.
* Analisar os itens solicitados pelos clientes, descritos logo abaixo:

| Grupo da lista de emails: |
|--------------------------------|
| Taxa da importância das discussões (se está em alta, ou está decrescendo)|
| O quão agressivas (ou amigáveis) são as respostas dos principais desenvolvedores |
| Tempo de vida da conversa |
| Participantes envolvidos |
| Volume de mensagens |

| Grupo do Git: |
|----------------------|
| Papel dos contribuidores (fazem commits ou reviews) |
| De onde os contribuidores são no mundo |
| Nível de experiência |
| De que empresa eles fazem parte |
| Tempo de contribuição |
| Como é a atividade de cada subsistema (no caso do Kernel do linux) |
| Quantidade de linhas modificadas |
| Quantidade de arquivos modificados |
| Evolução da atividade do usuário |
| Taxa de commits com a tag ‘fix’ |
| Autores com mais 'reviewed by' |
| Análise de commits (título, tags, diff) |
| Relação entre review e fix |

| Grupo do IRC: |
|----------------------|
| Influência dos usuários no canal |
| Atividade dos usuários no canal |
| Tempo de vida da conversa de acordo com o usuário envolvido |
| Período de atividade do canal (dia da semana, horário do dia) |
| Quantidade de usuários ao longo do tempo |
| Existência de participantes chave |

| Grupo do Issue tracker: |
|-----------------------------|
| Frequência dos tipos das issues criadas (labels) |
| Quantidade de PRs criados e aprovados |
| Quantidade de issues criadas |

## Exemplos de gráficos
![first_chart](uploads/lines_changed.png)
![second_chart] (uploads/contributors_come_from.png)
![third_chart] (uploads/issues_progress.png)
